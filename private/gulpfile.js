const gulp = require('gulp');
const babel = require('gulp-babel');
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
//need to run 'brew install libpng'
const imageminPngquant = require('imagemin-pngquant');
const imageminZopfli = require('imagemin-zopfli');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminGiflossy = require('imagemin-giflossy');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');


gulp.task('javascript',function(){
	gulp.src('./js/*.js')
    .pipe(plumber({errorHandler: notify.onError('<%= error.message %>')}))
    .pipe(babel())
		.pipe(uglify())
    .pipe(gulp.dest('../js/'));
});

gulp.task('sass', function () {
	gulp.src('./sass/*.scss')
    .pipe(sass().on('error', sass.logError))
		.pipe(cleanCSS())
		.pipe(postcss([ autoprefixer({
			grid: true
		})]))
    .pipe(gulp.dest('../css/'));
});

gulp.task('imagemin', function(){
    gulp.src('./img/*.*')
        .pipe(imagemin([
            imageminPngquant({
                speed: 1,
                quality: 90
            }),
            imageminZopfli({
                more: true
            }),
            imageminGiflossy({
                optimizationLevel: 3,
                optimize: 3,
                lossy: 2
            }),
            imagemin.svgo({
                plugins: [{
                    removeViewBox: false
                }]
            }),
            imagemin.jpegtran({
				quality: 90,
                progressive: true
            })
        ]))
        .pipe(gulp.dest('../img/'));
});

gulp.task('watch', function(){
    gulp.watch('./sass/*.scss', ['sass']);
		gulp.watch('./js/*.js', ['javascript']);
});

gulp.task('default', ['javascript','sass','watch']);
